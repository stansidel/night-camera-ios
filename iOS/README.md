# Night Camera

See <https://github.com/ekurutepe/iOS-OpenCV-FaceRec> for working with OpenCV in iOS.

Possibly useful links:

* <https://answers.opencv.org/question/13278/cant-detect-face-on-iphone-when-iphone-is-in-landscape-mode/>.
* <https://answers.opencv.org/question/22880/cvvideocamera-on-ios7/>.
* <https://www.toptal.com/machine-learning/real-time-object-detection-using-mser-in-ios>.
* <https://stackoverflow.com/questions/42102385/opencv-tone-curve-progrommatically>.
* <https://stackoverflow.com/questions/10254141/how-to-convert-from-cvmat-to-uiimage-in-objective-c/35999152#35999152>

Of some interest:

* <https://stackoverflow.com/questions/37051021/opencv-camera-preview-on-ios-stretched>.
* <https://stackoverflow.com/questions/21587816/memory-leak-uiimagefromcvmat>
* <https://stackoverflow.com/questions/35848889/ios-and-opencv-how-to-initialise-cvvideocamera-object-to-reflect-uiimage-size>

Performance:

* <https://stackoverflow.com/questions/7899108/opencv-get-pixel-channel-value-from-mat-image>.
