//
//  Metrics.swift
//  Buy App
//
//  Created by Stan Sidel on 21.11.2019.
//  Copyright © 2019 The Sidel Family. All rights reserved.
//

import Foundation

import Keys
import Amplitude_iOS

class Metrics {
    static let shared = Metrics(apiKey: NightCameraKeys().amplitudeApiKey)
    
    init(apiKey: String) {
        Amplitude.instance()?.initializeApiKey(apiKey)
    }
    
    func track(event name: String, properties: [String: String] = [:]) {
        Amplitude.instance()?.logEvent(name, withEventProperties: properties)
        #if DEBUG
        print("Tracked event \"\(name)\" with properties: \(properties)")
        #endif
    }
    
    func trackLaunch() {
        let launchSettingKey = "metrics.general.launched"
        if UserDefaults.standard.bool(forKey: launchSettingKey) {
            track(event: "launch")
        } else {
            track(event: "firstLaunch")
            UserDefaults.standard.set(true, forKey: launchSettingKey)
        }
    }
}
