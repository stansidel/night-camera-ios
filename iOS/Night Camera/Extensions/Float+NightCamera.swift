//
//  Float+NightCamera.swift
//  Night Camera
//
//  Created by Stanislav Sidelnikov on 9/7/19.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

import Foundation

extension Float {
    func rounded(toPlaces places: Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return roundf(self * divisor) / divisor
    }
}
