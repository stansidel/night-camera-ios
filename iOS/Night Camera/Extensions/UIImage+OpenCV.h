//
//  UIImage+OpenCV.h
//  Night Camera
//
//  Created by Stanislav Sidelnikov on 8/10/19.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (OpenCV)
+ (UIImage *)imageWithCVMat:(cv::Mat)cvMat;
@end

NS_ASSUME_NONNULL_END
