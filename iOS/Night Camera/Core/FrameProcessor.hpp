//
//  FrameProcessor.hpp
//  Night Camera
//
//  Created by Stan Sidel on 04/05/2019.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

#ifndef FrameProcessor_hpp
#define FrameProcessor_hpp

#include <string>
#include <queue>
#include <tuple>

#include <opencv2/opencv.hpp>

namespace NightCamera {
    class FrameProcessor {
    private:
        std::queue<cv::Mat> ProcessedImages_;
        cv::Mat CurrentImage_;
        double Contrast_ = 10.0;
        double Brightness_ = 10.0;
        bool AutoAdjustBrightnessAndContrast_ = true;
        size_t MaxProcessingStackSize_ = 20;
        bool ShouldOverlaySettings_ = true;
        bool ShouldOverlayPromoText_ = false;
        std::string PromoText_ = "";
        float LastRMSE_ = 0;
    public:
        FrameProcessor() = default;
        void ProcessFrame(cv::Mat& image);
        cv::Mat currentImage() const;
        double GetContrast() const;
        void SetContrast(double new_contrast);
        double GetBrightness() const;
        void SetBrightness(double new_brightness);
        size_t GetMaxProcessingStackSize() const;
        void SetMaxProcessingStackSize(size_t new_size);
        bool GetShouldOverlaySettings() const;
        void SetShouldOverlaySettings(bool new_value);
        bool GetAutoAdjustBrightnessAndContrast() const;
        void SetAutoAdjustBrightnessAndContrast(bool new_value);
        void EnablePromoTextOverlay(std::string text);
        void DisablePromoTextOverlay();
        bool IsPromoTextOverlayEnabled() const;
        std::tuple<double, double> OptimalBrightnessAndContrast(const cv::Mat& image) const;
    private:
        template<typename VecTypeC, typename VecTypeF>
        void UpdateCurrentImage(const cv::Mat& image);

        template<typename VecTypeC, typename VecTypeF>
        void ProcessImage(cv::Mat &image);

        void ShrinkStackToMaxSize();
        void AddSettingsToImage(cv::Mat& image) const;
        void AutoAdjustBrightnessAndContrast(const cv::Mat &image);
    };
}

#endif /* FrameProcessor_hpp */
