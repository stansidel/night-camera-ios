//
//  FrameProcessor.cpp
//  Night Camera
//
//  Created by Stan Sidel on 04/05/2019.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

#include "FrameProcessor.hpp"

#include <algorithm>
#include <climits>
#include <iostream>
#include <sstream>
#include <utility>

#define VEC_TYPE cv::Vec4b
#define VEC_TYPE_FLOAT cv::Vec4f

using namespace NightCamera;

// MARK: - Constants
const float MAX_BRIGHTNESS = 30;
const float MAX_CONTRAST = 30;

// MARK: - Local functions declarations
enum HorizontalPosition { left, right };
enum VerticalPosition { top, bottom };

template<typename VecTypeC>
void AdjustBrightnessAndContrast(cv::Mat &image, const double contrast = 1.0, const double brightness = 0);
void PutMultilineText(
                      const std::string& text,
                      cv::Mat& image,
                      VerticalPosition vertical_position = VerticalPosition::top,
                      HorizontalPosition horizontal_position = HorizontalPosition::left,
                      int hor_margin = 15,
                      int ver_margin = 15
                      );
template<typename VecTypeC, typename Callable>
void ProcessEachImagePoint(cv::Mat& image, Callable processor, double scale = 1);
template<typename VecTypeC, typename Callable>
void ProcessEachImagePoint(const cv::Mat& image, Callable processor, double scale);
template<typename VecTypeF>
float CalculateRMSE(const cv::Mat& avgImage, const size_t avgdOver, const cv::Mat& curFrame);
void BalanceColors(const cv::Mat& in, cv::Mat& out, const float percent);
std::tuple<double, double> CalculateOptimalBrightnessAndContrast(const cv::Mat &image, float clipHistPercent=0);

// MARK: - FrameProcessor definitions

void FrameProcessor::ProcessFrame(cv::Mat &image) {
    // See https://stackoverflow.com/questions/42102385/opencv-tone-curve-progrommatically
    if (image.channels() == 2) {
        ProcessImage<cv::Vec2b, cv::Vec2f>(image);
    } else if (image.channels() == 3) {
        ProcessImage<cv::Vec3b, cv::Vec3f>(image);
    } else if (image.channels() == 4) {
        ProcessImage<cv::Vec4b, cv::Vec4f>(image);
    } else {
        std::cerr << "The image contains " << image.channels() << " channels which is not supported." << '\n';
    }
}

template<typename VecTypeC, typename VecTypeF>
void FrameProcessor::ProcessImage(cv::Mat &image) {
    if (GetAutoAdjustBrightnessAndContrast()) {
        AutoAdjustBrightnessAndContrast(image);
    }
    AdjustBrightnessAndContrast<VecTypeC>(image, Brightness_, Contrast_);
    UpdateCurrentImage<VecTypeC, VecTypeF>(image);
}

template<typename VecType>
std::string _pixel(const cv::Mat& image, const int x, const int y, const size_t count = 1) {
    if (x >= image.cols || y >= image.rows) {
        return "error";
    }
    std::stringstream result("(");
    bool first = true;
    for (int c = 0; c < image.channels(); ++c) {
        if (!first) {
            result << ", ";
        }
        first = false;
        result << image.at<VecType>(y,x)[c];
    }
    result << ") / " << count;
    return result.str();
}

template<typename VecTypeC, typename VecTypeF>
void FrameProcessor::UpdateCurrentImage(const cv::Mat& image) {
    // https://stackoverflow.com/questions/6302171/convert-uchar-mat-to-float-mat-in-opencv
    cv::Mat floatImage;
    image.convertTo(floatImage, CV_32F);
    if (ProcessedImages_.size() == 0) {
        floatImage.copyTo(CurrentImage_);
        ProcessedImages_.push(floatImage);
        return;
    }
    ShrinkStackToMaxSize();

    LastRMSE_ = CalculateRMSE<VecTypeF>(CurrentImage_, ProcessedImages_.size(), floatImage);

    CurrentImage_ += floatImage;
    ProcessedImages_.push(floatImage);
}

cv::Mat FrameProcessor::currentImage() const {
    cv::Mat resultImage;
    if (ProcessedImages_.size() == 0) {
        return resultImage;
    }

    cv::normalize(CurrentImage_ / ProcessedImages_.size(), resultImage, 0, 255, cv::NormTypes::NORM_MINMAX, CV_8U);

    BalanceColors(resultImage, resultImage, 0.4);

    if (ShouldOverlaySettings_) {
        AddSettingsToImage(resultImage);
    }

    if (IsPromoTextOverlayEnabled()) {
        PutMultilineText(PromoText_, resultImage, VerticalPosition::bottom, HorizontalPosition::left);
    }

    return resultImage;
}

void FrameProcessor::AddSettingsToImage(cv::Mat& image) const {
    std::stringstream settings;
    settings << "Brightness: " << Brightness_ << '\n';
    settings << "Contrast: " << Contrast_ << '\n';
    settings << "Auto: " << (AutoAdjustBrightnessAndContrast_ ? "On" : "Off") << '\n';
    settings << "Stack Size: " << MaxProcessingStackSize_ << '\n';
    settings << "RMSE: " << LastRMSE_ << '\n';
    PutMultilineText(settings.str(), image);
}

std::tuple<double, double> FrameProcessor::OptimalBrightnessAndContrast(const cv::Mat &image) const {
    return CalculateOptimalBrightnessAndContrast(image, 20);
}

bool FrameProcessor::GetAutoAdjustBrightnessAndContrast() const {
    return AutoAdjustBrightnessAndContrast_;
}

void FrameProcessor::SetAutoAdjustBrightnessAndContrast(bool new_value) {
    AutoAdjustBrightnessAndContrast_ = new_value;
}

void FrameProcessor::AutoAdjustBrightnessAndContrast(const cv::Mat &image) {
    const auto [brightness, contrast] = OptimalBrightnessAndContrast(image);
    const double threshold = 0.05;

    if (abs(brightness) <= MAX_BRIGHTNESS && std::abs(GetBrightness() - brightness) > GetBrightness() * threshold) {
        SetBrightness(brightness);
    }
    if (abs(contrast) <= MAX_CONTRAST && std::abs(GetContrast() - contrast) > GetContrast() * threshold) {
        SetContrast(contrast);
    }
}

void FrameProcessor::EnablePromoTextOverlay(std::string text) {
    ShouldOverlayPromoText_ = true;
    PromoText_ = text;
}

void FrameProcessor::DisablePromoTextOverlay() {
    ShouldOverlayPromoText_ = false;
    PromoText_ = "";
}

bool FrameProcessor::IsPromoTextOverlayEnabled() const {
    return ShouldOverlayPromoText_ && !PromoText_.empty();
}

// MARK: - Local functions definitions

void PutMultilineText(
                      const std::string& text,
                      cv::Mat& image,
                      VerticalPosition vertical_position,
                      HorizontalPosition horizontal_position,
                      int hor_margin,
                      int ver_margin
                      ) {
    std::stringstream ss(text);
    std::string temp;
    const int fontFace = cv::FONT_HERSHEY_COMPLEX_SMALL;
    const double fontScale = 0.8;
    const int fontThickness = 1;
    const int margin = 5;
    int fullHeight = 0;
    int maxWidth = 0;
    std::vector<std::tuple<std::string, cv::Size>> lines;
    while (std::getline(ss, temp, '\n')) {
        int baseline = 0;
        cv::Size textSize = cv::getTextSize(temp, fontFace, fontScale, fontThickness, &baseline);
        lines.push_back({temp, textSize});
        fullHeight += textSize.height + margin;
        maxWidth = std::max(maxWidth, textSize.width);
    }
    int y = 0;
    int minX = 0;
    const int imgWidth = image.cols;
    const int imgHeight = image.rows;
    switch (horizontal_position) {
        case HorizontalPosition::left: minX = hor_margin; break;
        case HorizontalPosition::right: minX = imgWidth - maxWidth - hor_margin; break;
    }
    switch (vertical_position) {
        case VerticalPosition::top: y = ver_margin; break;
        case VerticalPosition::bottom: y = imgHeight - fullHeight - hor_margin; break;
    }
    for (const auto& [temp, textSize] : lines) {
        y += textSize.height;
        cv::rectangle(
                      image,
                      cv::Point(minX, y - textSize.height),
                      cv::Point(minX + textSize.width, y),
                      cv::Scalar(255, 255, 255),
                      cv::FILLED
                      );
        cv::putText(
                    image,
                    temp,
                    cv::Point(minX, y),
                    fontFace,
                    fontScale,
                    cv::Scalar(100, 100, 100),
                    fontThickness
                    );
        y += margin;
    }
}

// https://docs.opencv.org/3.4/d3/dc1/tutorial_basic_linear_transform.html
template<typename VecTypeC>
void AdjustBrightnessAndContrast(cv::Mat &image, const double brightness, const double contrast) {
    const double alpha = contrast;     /*< Simple contrast control */
    const double beta = brightness;       /*< Simple brightness control */
    ProcessEachImagePoint<VecTypeC>(image, [alpha, beta](uchar& point) { point = cv::saturate_cast<uchar>(alpha * point + beta); });
}

void FrameProcessor::ShrinkStackToMaxSize() {
    while (ProcessedImages_.size() >= MaxProcessingStackSize_) {
        CurrentImage_ -= ProcessedImages_.front();
        ProcessedImages_.pop();
    }
}

double FrameProcessor::GetContrast() const {
    return Contrast_;
}

void FrameProcessor::SetContrast(double new_contrast) {
    Contrast_ = new_contrast;
}

double FrameProcessor::GetBrightness() const {
    return Brightness_;
}

void FrameProcessor::SetBrightness(double new_brightness) {
    Brightness_ = new_brightness;
}

size_t FrameProcessor::GetMaxProcessingStackSize() const {
    return MaxProcessingStackSize_;
}

void FrameProcessor::SetMaxProcessingStackSize(size_t new_size) {
    MaxProcessingStackSize_ = new_size;
    ShrinkStackToMaxSize();
}

bool FrameProcessor::GetShouldOverlaySettings() const {
    return ShouldOverlaySettings_;
}

void FrameProcessor::SetShouldOverlaySettings(bool new_value) {
    ShouldOverlaySettings_ = new_value;
}

template<typename VecTypeC, typename Callable>
void ProcessEachImagePoint(cv::Mat& image, Callable processor, double scale) {
    for (int y = 0; y < image.rows * scale; y++) {
        for (int x = 0; x < image.cols * scale; x++) {
            for (int c = 0; c < image.channels(); c++) {
                processor(image.at<VecTypeC>(y,x)[c]);
            }
        }
    }
}

template<typename VecTypeC, typename Callable>
void ProcessEachImagePoint(const cv::Mat& image, Callable processor, double scale) {
    for (int y = 0; y < image.rows * scale; y++) {
        for (int x = 0; x < image.cols * scale; x++) {
            for (int c = 0; c < image.channels(); c++) {
                processor(image.at<VecTypeC>(y,x)[c]);
            }
        }
    }
}

template<typename VecTypeF>
float CalculateRMSE(const cv::Mat& avgImage, const size_t avgdOver, const cv::Mat& curFrame) {
    cv::Mat diff = avgImage / avgdOver - curFrame;
    double scale = 1;
    int size = (diff.cols * scale) * (diff.rows * scale) * diff.channels();
    float sum = 0;
    ProcessEachImagePoint<VecTypeF>(diff, [&sum](const float p) { sum += pow(p, 2); }, scale);
    return sqrt(sum) / (float)size;
}

/// perform the Simplest Color Balancing algorithm
void BalanceColors(const cv::Mat& in, cv::Mat& out, const float percent) {
    assert(percent > 0 && percent < 100);

    float half_percent = percent / 200.0f;

    std::vector<cv::Mat> tmpsplit; split(in,tmpsplit);
    const int channels = in.channels();
    for (int i=0; i < channels; i++) {
        //find the low and high precentile values (based on the input percentile)
        cv::Mat flat;
        tmpsplit[i].reshape(1, 1).copyTo(flat);
        cv::sort(flat, flat, CV_SORT_EVERY_ROW + CV_SORT_ASCENDING);
        int lowval = flat.at<uchar>(cvFloor(((float)flat.cols) * half_percent));
        int highval = flat.at<uchar>(cvCeil(((float)flat.cols) * (1.0 - half_percent)));

        //saturate below the low percentile and above the high percentile
        tmpsplit[i].setTo(lowval,tmpsplit[i] < lowval);
        tmpsplit[i].setTo(highval,tmpsplit[i] > highval);

        //scale the channel
        normalize(tmpsplit[i], tmpsplit[i], 0, 255, cv::NORM_MINMAX);
    }
    merge(tmpsplit, out);
}

/**
 *  \brief Automatic brightness and contrast optimization with optional histogram clipping
 *  \param [in]src Input image GRAY or BGR or BGRA
 *  \param clipHistPercent cut wings of histogram at given percent tipical=>1, 0=>Disabled
 *  \note In case of BGRA image, we won't touch the transparency
*/
std::tuple<double, double> CalculateOptimalBrightnessAndContrast(const cv::Mat &src, float clipHistPercent)
{

    CV_Assert(clipHistPercent >= 0);
    CV_Assert((src.type() == CV_8UC1) || (src.type() == CV_8UC3) || (src.type() == CV_8UC4));

    int histSize = 256;
    float alpha, beta;
    double minGray = 0, maxGray = 0;

    //to calculate grayscale histogram
    cv::Mat gray;
    if (src.type() == CV_8UC1) gray = src;
    else if (src.type() == CV_8UC3) cvtColor(src, gray, CV_BGR2GRAY);
    else if (src.type() == CV_8UC4) cvtColor(src, gray, CV_BGRA2GRAY);
    if (clipHistPercent == 0)
    {
        // keep full available range
        cv::minMaxLoc(gray, &minGray, &maxGray);
    }
    else
    {
        cv::Mat hist; //the grayscale histogram

        float range[] = { 0, 256 };
        const float* histRange = { range };
        bool uniform = true;
        bool accumulate = false;
        calcHist(&gray, 1, 0, cv::Mat (), hist, 1, &histSize, &histRange, uniform, accumulate);

        // calculate cumulative distribution from the histogram
        std::vector<float> accumulator(histSize);
        accumulator[0] = hist.at<float>(0);
        for (int i = 1; i < histSize; i++)
        {
            accumulator[i] = accumulator[i - 1] + hist.at<float>(i);
        }

        // locate points that cuts at required value
        float max = accumulator.back();
        clipHistPercent *= (max / 100.0); //make percent as absolute
        clipHistPercent /= 2.0; // left and right wings
        // locate left cut
        minGray = 0;
        while (accumulator[minGray] < clipHistPercent)
            minGray++;

        // locate right cut
        maxGray = histSize - 1;
        while (accumulator[maxGray] >= (max - clipHistPercent))
            maxGray--;
    }

    // current range
    float inputRange = maxGray - minGray;

    alpha = (histSize - 1) / inputRange;   // alpha expands current range to histsize range
    beta = -minGray * alpha;             // beta shifts current range so that minGray will go to 0
//    beta = maxGray * alpha - 255;

    return {beta, alpha};
}
