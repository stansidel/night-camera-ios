//
//  LiveCameraViewController.swift
//  Night Camera
//
//  Created by Stanislav Sidelnikov on 9/7/19.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

import UIKit

class LiveCameraViewController: UIViewController {
    @IBOutlet private var resultView: UIImageView!
    @IBOutlet private var cameraView: UIImageView!

    private var cameraInteractor: SSCameraInteractor!

    override func viewDidLoad() {
        super.viewDidLoad()
        cameraInteractor = SSCameraInteractor(cameraView: cameraView, resultView: resultView)
        cameraInteractor.promoText = "Taken with Night Camera app.\nhttps://nc.app.sidel.family."
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        processAppearing()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        processDisappearing()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "ShowSettings", let settingsVC = segue.destination as? SettingsViewController {
            settingsVC.setInitial(
                brightness: cameraInteractor.brightness,
                contrast: cameraInteractor.contrast,
                autoBrightnessAndContrast: cameraInteractor.autoAdjustBrightnessAndContrast,
                maxFramesStackSize: cameraInteractor.maxProcessingFrames
            )
            settingsVC.onClose = { [weak cameraInteractor] viewController in
                guard let scameraInteractor = cameraInteractor else {
                    return
                }
                scameraInteractor.brightness = viewController.brightness
                scameraInteractor.contrast = viewController.contrast
                scameraInteractor.autoAdjustBrightnessAndContrast = viewController.autoBrightnessAndContrast
                scameraInteractor.maxProcessingFrames = viewController.maxFramesStackSize
                viewController.dismiss(animated: true, completion: nil)
            }
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    @IBAction private func recordButtonTapped(_ sender: Any) {
        cameraInteractor.saveToCameraRoll()
        Metrics.shared.track(
            event: "liveCameraView.recordTapped",
            properties: [
                "brightness": String(cameraInteractor.brightness),
                "contrast": String(cameraInteractor.contrast),
                "auto": String(cameraInteractor.autoAdjustBrightnessAndContrast),
                "maxFrames": String(cameraInteractor.maxProcessingFrames)
            ]
        )
    }

    private func processAppearing() {
        cameraInteractor.startCapture()
    }

    private func processDisappearing() {
        cameraInteractor.stopCapture()
    }
}
