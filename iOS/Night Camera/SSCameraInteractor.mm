//
//  SSCameraInteractor.m
//  Night Camera
//
//  Created by Stan Sidel on 04/05/2019.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

#import <opencv2/videoio/cap_ios.h>

#import "SSCameraInteractor.h"
#import "Core/FrameProcessor.hpp"
#import "Extensions/UIImage+OpenCV.h"

@interface SSCameraInteractor ()<CvVideoCameraDelegate> {
    cv::Mat _LatestOrigFrame;
    cv::Mat _LatestProcessedFrame;
}

@property (nonatomic, strong) CvVideoCamera* videoCamera;

@property (nonatomic, assign) NightCamera::FrameProcessor *frameProcessor;

@property (nonatomic, weak) UIImageView *resultView;

@end

@implementation SSCameraInteractor

- (instancetype)initWithCameraView:(UIImageView *)view resultView:(UIImageView *)resultView {
    self = [super init];
    if (!self) {
        return self;
    }
    
    self.videoCamera = [[CvVideoCamera alloc] initWithParentView:view];
    self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionBack;
    self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset640x480;
    self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
    self.videoCamera.defaultFPS = 30;
    self.videoCamera.grayscaleMode = NO;
    self.videoCamera.delegate = self;
    self.videoCamera.rotateVideo = YES;

    self.resultView = resultView;

    NightCamera::FrameProcessor *frameProcessor = new NightCamera::FrameProcessor();
    #ifdef DEBUG
    frameProcessor->SetShouldOverlaySettings(true);
    #else
    frameProcessor->SetShouldOverlaySettings(false);
    #endif
    self.frameProcessor = frameProcessor;
    
    return self;
}

- (void)saveToCameraRoll {
    cv::Mat resultImage = self.frameProcessor->currentImage();
    cv::Mat latestOrigFrame = _LatestOrigFrame;
    cv::Mat latestProcessedFrame = _LatestProcessedFrame;

    dispatch_async(dispatch_get_main_queue(), ^{
        #ifdef DEBUG
        UIImageWriteToSavedPhotosAlbum(
                                       [UIImage imageWithCVMat:latestOrigFrame],
                                       nil,
                                       nil,
                                       nil
                                       );
        UIImageWriteToSavedPhotosAlbum(
                                       [UIImage imageWithCVMat:latestProcessedFrame],
                                       nil,
                                       nil,
                                       nil
                                       );
        #endif
        UIImageWriteToSavedPhotosAlbum(
                                       [UIImage imageWithCVMat:resultImage],
                                       nil,
                                       nil,
                                       nil
                                       );
    });

}

- (void)startCapture {
    [self.videoCamera start];
}

- (void)stopCapture {
    [self.videoCamera stop];
}

- (void)processImage:(cv::Mat&)image {
    image.copyTo(_LatestOrigFrame);
    // Do some OpenCV stuff with the image
    self.frameProcessor->ProcessFrame(image);

    image.copyTo(_LatestProcessedFrame);

    cv::Mat resultImage = self.frameProcessor->currentImage();

    dispatch_async(dispatch_get_main_queue(), ^{
        self.resultView.image = [UIImage imageWithCVMat:resultImage];
    });
}

- (double)contrast {
    return self.frameProcessor->GetContrast();
}

- (void)setContrast:(double)contrast {
    self.frameProcessor->SetContrast(contrast);
}

- (double)brightness {
    return self.frameProcessor->GetBrightness();
}

- (void)setBrightness:(double)brightness {
    self.frameProcessor->SetBrightness(brightness);
}

- (unsigned long)maxProcessingFrames {
    return self.frameProcessor->GetMaxProcessingStackSize();
}

- (void)setMaxProcessingFrames:(unsigned long)maxProcessingFrames {
    self.frameProcessor->SetMaxProcessingStackSize(maxProcessingFrames);
}

- (bool)autoAdjustBrightnessAndContrast {
    return self.frameProcessor->GetAutoAdjustBrightnessAndContrast();
}

- (void)setAutoAdjustBrightnessAndContrast:(bool)autoAdjustBrightnessAndContrast {
    self.frameProcessor->SetAutoAdjustBrightnessAndContrast(autoAdjustBrightnessAndContrast);
}

- (void)setPromoText:(NSString *)promoText {
    _promoText = promoText;
    if (promoText != nil && [promoText length] > 0) {
        self.frameProcessor->EnablePromoTextOverlay([promoText UTF8String]);
    } else {
        self.frameProcessor->DisablePromoTextOverlay();
    }
}

- (void)dealloc
{
    delete self.frameProcessor;
    // Shouldn't be called when using ARC
    // [super dealloc];
}
@end
