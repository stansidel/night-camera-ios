//
//  SettingsViewController.swift
//  Night Camera
//
//  Created by Stanislav Sidelnikov on 9/7/19.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    var onClose: ((SettingsViewController) -> Void)?
    private(set) var brightness: Double = 0.0
    private(set) var contrast: Double = 0.0
    private(set) var autoBrightnessAndContrast: Bool = false
    private(set) var maxFramesStackSize: UInt = 1

    func setInitial(brightness: Double, contrast: Double, autoBrightnessAndContrast: Bool, maxFramesStackSize: UInt) {
        self.brightness = brightness
        self.contrast = contrast
        self.autoBrightnessAndContrast = autoBrightnessAndContrast
        self.maxFramesStackSize = maxFramesStackSize
        updateUI(withSliders: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        contrastSlider.minimumValue = 0.0
        contrastSlider.maximumValue = 20.0

        brightnessSlider.minimumValue = -20.0
        brightnessSlider.maximumValue = 20.0

        maxFramesStackSizeSlider.minimumValue = 1
        maxFramesStackSizeSlider.maximumValue = 100

        updateUI(withSliders: true)
        
        Metrics.shared.track(event: "settingsView.didLoad")
    }

    // MARK: Private
    @IBOutlet private var contrastValueLabel: UILabel!
    @IBOutlet private var contrastSlider: UISlider!
    @IBOutlet private var brightnessValueLabel: UILabel!
    @IBOutlet private var brightnessSlider: UISlider!
    @IBOutlet private var maxFramesStackSizeValueLabel: UILabel!
    @IBOutlet private var maxFramesStackSizeSlider: UISlider!
    @IBOutlet private var autoBrightnessAndContrastSwitch: UISwitch!

    @IBAction private func changedContrast(_ sender: UISlider) {
        contrast = Double(sender.value.rounded(toPlaces: 2))
        updateUI(withSliders: false)
        Metrics.shared.track(
            event: "settingsView.contrastChanged",
            properties: ["value": String(contrast)]
        )
    }

    @IBAction private func switchedAutoBrightnessAndContrast(_ sender: Any) {
        autoBrightnessAndContrast = autoBrightnessAndContrastSwitch.isOn
        updateUI(withSliders: false)
        Metrics.shared.track(
            event: "settingsView.autoSwitched",
            properties: ["isOn": String(autoBrightnessAndContrast)]
        )
    }

    @IBAction private func changedBrightness(_ sender: UISlider) {
        brightness = Double(sender.value.rounded(toPlaces: 2))
        updateUI(withSliders: false)
        Metrics.shared.track(
            event: "settingsView.brightnessChanged",
            properties: ["value": String(brightness)]
        )
    }

    @IBAction private func changedMaxFramesStackSize(_ sender: UISlider) {
        maxFramesStackSize = UInt(sender.value.rounded())
        updateUI(withSliders: false)
        Metrics.shared.track(
            event: "settingsView.maxFramesChanged",
            properties: ["value": String(maxFramesStackSize)]
        )
    }

    @IBAction func closeWindow(_ sender: Any) {
        onClose?(self)
    }

    private func updateUI(withSliders updateSliders: Bool) {
        guard isViewLoaded else {
            return
        }
        contrastValueLabel.text = String(format: "%.2f", contrast)
        brightnessValueLabel.text = String(format: "%.2f", brightness)
        maxFramesStackSizeValueLabel.text = String(maxFramesStackSize)

        contrastValueLabel.isEnabled = !autoBrightnessAndContrast
        brightnessValueLabel.isEnabled = !autoBrightnessAndContrast
        contrastSlider.isEnabled = !autoBrightnessAndContrast
        brightnessSlider.isEnabled = !autoBrightnessAndContrast

        if updateSliders {
            contrastSlider.value = Float(contrast)
            brightnessSlider.value = Float(brightness)
            maxFramesStackSizeSlider.value = Float(maxFramesStackSize)
            autoBrightnessAndContrastSwitch.isOn = autoBrightnessAndContrast
        }
    }
}
