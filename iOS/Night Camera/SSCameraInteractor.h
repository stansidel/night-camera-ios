//
//  SSCameraInteractor.h
//  Night Camera
//
//  Created by Stan Sidel on 04/05/2019.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SSCameraInteractor : NSObject
- (instancetype)initWithCameraView:(UIImageView *)view resultView:(UIImageView *)resultView;

- (void)startCapture;
- (void)stopCapture;
- (void)saveToCameraRoll;

@property (nonatomic, assign) bool autoAdjustBrightnessAndContrast;
@property (nonatomic, assign) double contrast;
@property (nonatomic, assign) double brightness;
@property (nonatomic, assign) unsigned long maxProcessingFrames;
@property (nonatomic, assign) NSString* promoText;
@end

NS_ASSUME_NONNULL_END
