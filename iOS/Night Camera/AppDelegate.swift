//
//  AppDelegate.swift
//  Night Camera
//
//  Created by Stan Sidel on 05.12.2019.
//  Copyright © 2019 Stan Sidel. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Metrics.shared.trackLaunch()
        return true
    }

}
