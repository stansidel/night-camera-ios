---
title: "About The Night Shot App"
date: 2019-11-27T20:17:56+05:00
draft: false
---

Sleeping kids are cute. But it's so hard<sup>*</sup> to take pictures of them at night. The Night Shot App aims at solving that part.

You can take a shot of anything in low light, like a room with night light. Just aim the camera at the object, hold steady for several seconds, and tap the red button. The app continuously takes shots and adjusts their brightness and contrast. Then it averages them to remove noise. You can find the final image in the Camera Roll.

The app automatically adjusts brightness and contrast. If you find the image is of low quality, go to settings, turn off the Auto switch and play with the settings. You can increase the max frame size to reduce noise at the expense of having to hold the device steady for longer.

The project is open-source and located at https://gitlab.com/stansidel/night-camera-ios/.

\* If you have iPhone 11, this might be a much lesser issue for you. 
