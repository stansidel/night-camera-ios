# Running it locally

Use docker to try the site localy as the built results may differ from what you get with just hugo server.

Run it:

```bash
docker build -t sidel-family/hugo .  # Need to be done once
# If docker-machine
docker run --rm -p 1313:1313 -v `realpath ./site`:/usr/share/blog -e HUGO_BASE_URL="http://$(docker-machine ip $(docker-machine active)):1313" sidel-family/hugo

# If local docker
docker run --rm -p 1313:1313 -v `realpath ./site`:/usr/share/blog -e HUGO_BASE_URL="http://localhost:1313" sidel-family/hugo
```

```fish
docker build -t sidel-family/hugo .  # Need to be done once
# If docker-machine
docker run --rm -p 1313:1313 -v (realpath ./site):/usr/share/blog -e HUGO_BASE_URL="http://"(docker-machine ip (docker-machine active))":1313" sidel-family/hugo

# If local docker
docker run --rm -p 1313:1313 -v (realpath ./site):/usr/share/blog -e HUGO_BASE_URL="http://localhost:1313" sidel-family/hugo
```
